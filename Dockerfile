FROM openjdk:8-alpine
ADD /target/TS.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]