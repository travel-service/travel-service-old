# Travel Service 
Simple landing page for travel agency with
## Environments
**PROD** http://kirovtravel-service.ru - основной сайт <br>
**DEV** http://kirovtravel-service.ru:8080 - сайт для раработки<br>
## Docker 

## CI-CD
* Pipelines configuration located in ***.gitlab-ci.yml***<br>
* To view gitlab pipelines visit https://gitlab.com/travel-service/travel-service-old/-/pipelines
* Pipelines Steps: 
1) build & test 
2) build docker image 
3) deploy to server with docker-compose
* Gitlab runner located on same machine with running application.
* All commits in 'master' branch automatically build docker image and deploy in PROD env with ***docker-compose.yaml*** configuration.
* All commits in not 'master' branches automatically build docker image and deploy in DEV env with ***docker-compose-dev.yaml*** configuration.
* To view container statuses and logs - visit portainer http://kirovtravel-service.ru:9000 
## Development rules
Commits only allowed to branches:
* feature/... - for new functionality
* bugfix/... - for bugs
* tech/... for infrastructure improvements
####To merge you branch in 'master' you need to create merge request in gitlab and be approved by Egor Okhapkin.