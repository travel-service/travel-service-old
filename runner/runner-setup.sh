#!/bin/bash
sudo gitlab-runner register -n \
  --url https://gitlab.com/ \
  --registration-token n7z2Az7ghgqn897gcoXL \
  --executor "docker" \
  --docker-image "docker:latest" \
  --description "My TS Runner" \
  --docker-volumes /var/run/docker.sock:/var/run/docker.sock

sudo usermod -aG docker gitlab-runner
sudo -u gitlab-runner -H docker info
