package ru.ohapegor.travelservice.config;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.net.URL;
import java.util.concurrent.TimeUnit;

@Configuration
@Slf4j
public class PhantomJsDriverConfig {

    @Value("${phantomjs.driver.url:http://127.0.0.1:8910}")
    private String driverUrl;

    @Value("${phantomjs.render.timeout:25}")
    private int pageTimeoutSec;

    @Bean
    @SneakyThrows
    public WebDriver driver() {
        log.info("configuring remote phantomjs web driver on url : {}", driverUrl);
        WebDriver driver = new RemoteWebDriver(new URL(driverUrl), DesiredCapabilities.phantomjs());
        driver.manage().timeouts()
                .pageLoadTimeout(pageTimeoutSec, TimeUnit.SECONDS)
                .implicitlyWait(pageTimeoutSec, TimeUnit.SECONDS)
                .setScriptTimeout(pageTimeoutSec, TimeUnit.SECONDS);
        return driver;
    }



}
