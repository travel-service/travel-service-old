package ru.ohapegor.travelservice.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Locale;

@Entity
@Table(name = "news")
public class NewsItem extends AbstractBaseEntity {

    private static DateTimeFormatter datePattern = new DateTimeFormatterBuilder()
            .appendPattern("dd MMMM yyyy")
            .toFormatter(new Locale("ru", "RU"));

    private static DateTimeFormatter timePattern = new DateTimeFormatterBuilder()
            .appendPattern("HH:mm")
            .toFormatter(new Locale("ru", "RU"));

    @Column(name = "title", nullable = false, unique = false)
    private String title;

    @Column(name = "date_time", nullable = false, columnDefinition = "timestamp")
    private LocalDateTime dateTime;

    @Column(name = "text", nullable = false, length = 2000)
    private String text;

    @Column(name = "image_href", nullable = false)
    private String imageHref;

    @Column(name = "href", nullable = false)
    private String href;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LocalDateTime getDateTime() {
        return dateTime;
    }

    public void setDateTime(LocalDateTime dateTime) {
        this.dateTime = dateTime;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getImageHref() {
        return imageHref;
    }

    public void setImageHref(String imageHref) {
        this.imageHref = imageHref;
    }

    public String getHref() {
        return href;
    }

    public void setHref(String href) {
        this.href = href;
    }

    public String printDate() {
        return dateTime.format(datePattern);
    }

    public String printTime() {
        return dateTime.format(timePattern);
    }

    @Override
    public String toString() {
        return "NewsItem{" +
                "\n\ttitle='" + title + '\'' +
                ", \n\tdateTime=" + dateTime +
                ", \n\ttext='" + text + '\'' +
                ", \n\timageHref='" + imageHref + '\'' +
                ", \n\thref='" + href + '\'' +
                ", \n\tid=" + id +
                "\n}";
    }
}
