package ru.ohapegor.travelservice.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import ru.ohapegor.travelservice.service.news.NewsService;
import ru.ohapegor.travelservice.service.tour.TourService;

import javax.annotation.PostConstruct;

import static ru.ohapegor.travelservice.util.LoggingUtil.enterInfo;
import static ru.ohapegor.travelservice.util.LoggingUtil.exitInfo;

@Service
@Profile("!test")
public class ScheduledLoader {

    private static final Logger logger = LoggerFactory.getLogger(ScheduledLoader.class);

    private final TourService tourService;

    private final NewsService newsService;

    public ScheduledLoader(NewsService newsService, TourService tourService) {
        this.newsService = newsService;
        this.tourService = tourService;
    }

    @PostConstruct
    void postConstruct() {
        logger.info(enterInfo());
        logger.info("reload = " + newsService.reloadNews());
    }

    //@Scheduled(cron = "30 */15 * * * *")
    public void fillDb(){
        logger.info(enterInfo());
        tourService.reloadTours();
        logger.info(exitInfo());
    }

    @Scheduled(cron = "0 */45 * * * *")
    public void loadNews(){
        logger.info(enterInfo());
        newsService.reloadNews();
        logger.info(exitInfo());
    }
}
