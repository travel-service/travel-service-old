package ru.ohapegor.travelservice.service.loader;

import org.jsoup.nodes.Document;

public interface HtmlLoader {

    Document renderPage(String url);
}
