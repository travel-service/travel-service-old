package ru.ohapegor.travelservice.service.loader;

import lombok.RequiredArgsConstructor;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import ru.ohapegor.travelservice.entities.NewsItem;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

import static ru.ohapegor.travelservice.util.LoggingUtil.enterInfo;
import static ru.ohapegor.travelservice.util.LoggingUtil.exitInfo;

@Service
@RequiredArgsConstructor
public class NewsLoader {

    private static final Logger logger = LoggerFactory.getLogger(TourLoader.class.getSimpleName());

    private static final String ELEMENT_CLASS = "n-news__item";
    private static final String DATE_CLASS = "n-news__date";
    private static final String TITLE_TAG = "n-news__tag";
    private static final String TEXT_CLASS = "n-news__title";
    private static final String IMAGE_CLASS = "n-news__img";

    private static final Locale ruLocale = new Locale("ru", "RU");

    private static final List<DateTimeFormatter> datePatterns = Arrays.asList(
            DateTimeFormatter.ofPattern("dd MMMM yyyy",ruLocale),
            DateTimeFormatter.ofPattern("d MMMM yyyy",ruLocale)
    );

    @Value("${news.url}")
    private  String newsUrl;

    private final HtmlLoader htmlLoader;

    public List<NewsItem> getNewsFromSite() {
        logger.info(enterInfo(newsUrl));
        List<NewsItem> news = null;
        try {
            Document doc = htmlLoader.renderPage(newsUrl);
            Elements elements = doc.getElementsByClass(ELEMENT_CLASS);
            news = elements.stream()
                    .map(this::parseElem)
                    // .peek(System.out::println)
                    .collect(Collectors.toList());
            logger.info(exitInfo("news size = " + news.size()));
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
            throw new ParseHtmlException(e);
        }
        logger.info(exitInfo());
        return news;
    }

    private NewsItem parseElem(Element element) {
        logger.info(enterInfo());
        NewsItem newsItem = null;
        //     System.out.println(element);
        try {
            //DateTime
            Element dateElement = element.getElementsByClass(DATE_CLASS).first();
            String date = dateElement.text();
            logger.debug("datetime = " + date);
            LocalDateTime localDateTime = parseNewsDate(date);

            //Title
            Element titleElement = element.getElementsByClass(TITLE_TAG).first();
            String title = titleElement.text();

            //Text
            Element textElement = element.getElementsByClass(TEXT_CLASS).first();
            String text = textElement.text();

            //Href
            String href = textElement.getElementsByTag("a").first().attr("href").trim();

            //Image
            Element imageElem = element.getElementsByClass(IMAGE_CLASS).first();
            String imageHref = imageElem.attr("style").trim()
                    .replaceAll("background-image: url\\('","")
                    .replaceAll("'\\)","");
            logger.debug("image url = " + imageHref);


            newsItem = new NewsItem();
            newsItem.setDateTime(localDateTime);
            newsItem.setTitle(title);
            newsItem.setText(text);
            newsItem.setImageHref(imageHref);
            newsItem.setHref(href);
        } catch (Exception e) {
            logger.error(e.getMessage(), e);
        }
        logger.info(exitInfo());
        return newsItem;
    }


    private static LocalDateTime parseNewsDate(String timeStr) {
        String trimString = timeStr.replaceAll("г\\.","");
        LocalDateTime parsedDate = null;
        for (DateTimeFormatter patten: datePatterns) {
            try {
                parsedDate = LocalDate.parse(trimString, patten).atStartOfDay();
            } catch (Exception e) {
                logger.info(datePatterns + " - invalid pattern for " + trimString + " ex:"+e);
            }
        }
        if (parsedDate == null) {
            throw new IllegalArgumentException("unparsable date "+timeStr);
        }
        return parsedDate;
    }
}
