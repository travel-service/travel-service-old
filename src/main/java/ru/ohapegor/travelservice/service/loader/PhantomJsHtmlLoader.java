package ru.ohapegor.travelservice.service.loader;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import static ru.ohapegor.travelservice.util.LoggingUtil.enterInfo;

@Service
public class PhantomJsHtmlLoader implements HtmlLoader {

    private static final Logger logger = LoggerFactory.getLogger(PhantomJsHtmlLoader.class);

    private final WebDriver driver;

    @Value("${phantomjs.render.retry-count:3}")
    private int retryCount;

    public PhantomJsHtmlLoader(WebDriver driver) {
        this.driver = driver;
    }

    public Document renderPage(String url) {
        logger.info(enterInfo());
        int count = 0;
        while (count < retryCount) {
            try {
                logger.info("Iteration - " + ++count);
                driver.get(url);
                String pageSource = driver.getPageSource();
                logger.trace("page source : {}", pageSource);
                return Jsoup.parse(driver.getPageSource());
            } catch (Exception e) {
                logger.error(e.toString());
                if (count >= retryCount) throw e;
            }
        }
        return null;
    }

}
