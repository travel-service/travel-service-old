package ru.ohapegor.travelservice.service.loader;

import lombok.SneakyThrows;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import ru.ohapegor.travelservice.TravelserviceApplication;

import java.io.File;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@SpringBootTest(classes = TravelserviceApplication.class)
class NewsLoaderTest {

    @Autowired
    private NewsLoader loader;

    @MockBean
    private HtmlLoader htmlLoader;

    @Test
    @SneakyThrows
    public void loadNewsSuccess(){
        File input = new File("src/test/resources/test-html/news.html");
        Document doc = Jsoup.parse(input, "UTF-8", "http://example.com/");
        when(htmlLoader.renderPage(any())).thenReturn(doc);
        System.out.println(doc.html());
    }

}